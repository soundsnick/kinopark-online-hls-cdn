import ffmpeg from 'fluent-ffmpeg';
import ffmpegInstaller from '@ffmpeg-installer/ffmpeg';
import {api} from "./api";
import axios from "axios";
import * as http from "http";

ffmpeg.setFfmpegPath(ffmpegInstaller.path);

export const generateChunks = async (id: string, filepath: string) => {
    // mp4
    ffmpeg(__dirname + '/../../uploads/' + filepath, { timeout: 432000 }).addOptions([
        '-profile:v baseline',
        '-level 3.0',
        '-start_number 0',
        '-hls_time 10',
        '-hls_list_size 0',
        '-f hls'
    ]).output(__dirname + `/../../uploads/${filepath}.m3u8`).on('end', () => { // m3u8
        console.log(`${id} processed successfully`)
        const accRequest = http.request({
            host: 'localhost',
            port: 3000,
            path: `/api/film/${id}/ready`,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        }, (res) => {
            res.on('data', (data) => console.log(data));
        });
        accRequest.write(JSON.stringify({ filename: filepath }));
        accRequest.end();
    }).run();
}
