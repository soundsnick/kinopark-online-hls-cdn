import {Request} from "express";

export const getCategory = (req: Request) => {
    const category = req.query.category;

    if (category) {
        return `${category}/`;
    } else return '';
}