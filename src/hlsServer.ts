import dotenv from 'dotenv';
// @ts-ignore
import HlsServer from 'hls-server';
// @ts-ignore
import httpAttach from 'http-attach';
import * as http from "http";

dotenv.config();

// @ts-ignore
const corsMiddleware = (req, res, next) => {
    // set your headers here
    res.setHeader('Access-Control-Allow-Origin', '*');
    next()
}

const port = process.env.SERVER_PORT || 3003;

const server = http.createServer();

const hls = new HlsServer(server, {
    path: "/streams",
    dir: __dirname + '/../uploads'
});

httpAttach(server, corsMiddleware);

server.listen(3003);
