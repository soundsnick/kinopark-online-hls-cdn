import {Request, Response} from "express";
import {authenticate, generateToken, getCategory} from "../utils";
import {generateChunks} from "../utils/generateChunks";

export const upload = async (req: Request, res: Response) => {
    try {
        if (authenticate(req)) {
            const id = req.params?.id;
            const category = getCategory(req);
            if(!req.files) {
                res.status(422).json({
                    message: 'no_file'
                });
            } else {
                const file = req.files?.file;

                if (file) {
                    if (!Array.isArray(file)) {
                        const newFileName = `${generateToken(15)}${file.name}`;
                        console.log(`UPLOAD | ./uploads/${category}` + newFileName)
                        file.mv(`./uploads/${category}` + newFileName)
                            .then(async () => {
                                await generateChunks(id, `${category}${newFileName}`)
                                return res.send(`${category}${newFileName}`);
                            })
                            .catch((err) => {
                                console.log(err);
                                return res.status(500).send(err);
                            });
                    } else {
                        return res.status(422).json({
                            message: "single_file",
                        });
                    }
                } else {
                    return res.status(422).json({
                        message: 'no_file'
                    });
                }
            }
        } else {
            return res.status(401).json({
                message: "unauthorized",
            });
        }
    } catch (err) {
        console.log(err);
        res.status(500).send(err);
    }
}
