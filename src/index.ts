import express from "express";
import { json, urlencoded } from "body-parser";
import cors from "cors";
import figlet from "figlet";
import {routes} from "./routes";
import fileUpload from "express-fileupload";
import dotenv from 'dotenv';

const app = express();

dotenv.config();

app.set('uploads', __dirname + '/uploads');

app.use(cors());
app.use(json());
app.use(urlencoded({extended: true}));

app.use(fileUpload({
    createParentPath: true
}));

// app.use(express.static('uploads'));

routes(app);

const port = process.env.SERVER_PORT || 3002;

app.listen(Number(port), () => {
    figlet("Kinopark HLS CDN", (err, data) => {
        if (err === null) {
            console.log("_____________");
            console.log("\x1b[32m%s\x1b[0m", data);
            console.log(`All services started, \x1b[32mhttp://localhost:${ port }\x1b[0m`);
        }
    });
});
